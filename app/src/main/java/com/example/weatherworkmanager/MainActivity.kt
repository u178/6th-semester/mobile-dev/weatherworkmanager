package com.example.weatherworkmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.work.*
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    lateinit var spinner: Spinner;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner = findViewById(R.id.spinner)
        val cities = resources.getStringArray(R.array.cities)
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item, cities)
        spinner.adapter = adapter
    }

    suspend fun loadWeather() {
        val API_KEY = resources.getString(R.string.api_key)
        var city = "Irkutsk"
        city = spinner.getSelectedItem().toString();


        val myData: Data = workDataOf("city" to city, "api_key" to API_KEY)
        val weatherWork = OneTimeWorkRequestBuilder<MyWorkManager>()
            .setInputData(myData)
            .build()


        WorkManager.getInstance(this).enqueue(weatherWork)
//        Log.d("mytag", "${weatherWork.toString()}")



//        val workiInfo = WorkManager.getInstance(this).getWorkInfoByIdLiveData(weatherWork.id).out
//        Log.d("mytag", "$workInfo")



//        val a = WorkManager.getInstance(this)
//            .beginWith(
//                workRequest.build())
//            .enqueue()

//        val workInfo = WorkManager.getInstance().getWorkInfoById().get()
//        Log.d("mytag", workRequest.toString())
    }


//        withContext(Dispatchers.Main) {
//            tempView.text = temp
//            descView.text = desc
//            wsView.text = wind_speed
//            wdView.text = wind_direction
//        }
//        Log.d("mytag", "$temp, $desc, $wind_speed, $wind_direction")

        // TODO: предусмотреть обработку ошибок (нет сети, пустой ответ)

    public fun onClick(v: View) {

        // Используем IO-диспетчер вместо Main (основного потока)
        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }
}


