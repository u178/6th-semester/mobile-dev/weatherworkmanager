package com.example.weatherworkmanager

import android.content.Context
import android.util.Log
import androidx.work.Data
import androidx.work.WorkerParameters
import androidx.work.Worker
import androidx.work.workDataOf
import org.json.JSONObject
import java.io.InputStream
import java.net.URL
import java.util.*


class MyWorkManager(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    override fun doWork(): Result {

        Log.d("mytag", "Working")

        return try {
            val city =  inputData.getString("city")
            val key =  inputData.getString("api_key")
            Log.d("mytag", "fetching")
            val weatherURL = "http://api.weatherapi.com/v1/current.json?key=$key&q=$city&aqi=no";
            //Log.d("mytag", weatherURL)
            val stream = URL(weatherURL).getContent() as InputStream
            Log.d("mytag", "fetched")

            // JSON отдаётся одной строкой,
            val data = Scanner(stream).nextLine()
            Log.d("mytag", data)

            val json = JSONObject(data).getJSONObject("current")
            val temp = json.getString("temp_c")
            val desc = json.getJSONObject("condition").getString("text")
            val wind_speed = json.getString("wind_kph")
            val wind_direction = json.getString("wind_dir")
            Log.d("mytag", "$temp, $desc, $wind_speed, $wind_direction")

            Log.d("mytag", "Finished working")

            val myData: Data = workDataOf("temp" to temp,
                "desc" to desc,
                "ws" to wind_speed,
                "wd" to wind_direction)

            Result.success(myData)
        } catch (error: Throwable) {
            Log.e("mytag", "Error: $error")
            Result.failure()
        }
    }
}
